package com.junit.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.log4j.sample.JunitDemo;

public class JunitDemoTest {
	
	@Test
	public void testGetString(){
		JunitDemo jd=new JunitDemo();
		
		String res=jd.getString();
		
		assertEquals("abc", res);
		
	}
	
	//@Test
	public void testGetNumber(){
		JunitDemo jd=new JunitDemo();
		int num=jd.getNumber();
		
		assertEquals(1, num);
		
	}

	

}
