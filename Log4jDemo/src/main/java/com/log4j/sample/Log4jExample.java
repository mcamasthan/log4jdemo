package com.log4j.sample;

import org.apache.log4j.Logger;

public class Log4jExample {
	final static Logger logger = Logger.getLogger(Log4jExample.class);

	public static void main(String[] args) {
		
		System.out.println(" this is sys out======");
		// db result
		//if(result!=null){
		logger.info("This is info........");
		//}
		logger.warn("This is warn : " );
		logger.error("This is error : " );
		logger.fatal("This is fatal : " );
		logger.debug("This is debug : ");
		logger.trace("This is trace : ");
		 
		// 100 -> TRACE, 25 debug 25 5 debug 99 
		
		// dao lo error , clas-> 19 , null , sequce
		
		// time, class method, line, messge , console 100, more than 500 , logs ,file , error , one , log4j , logs classfication
		
		//DEBUG - it will print all logs
		// INFO - it will all logs except debug
		// warn-> it will skip debug,info
		//fatal-> it will print only fatal
		// error-> it will print error and fatal
		
//ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < OFF  
	}

}
