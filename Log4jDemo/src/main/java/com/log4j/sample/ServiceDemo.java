package com.log4j.sample;

import org.apache.log4j.Logger;

public class ServiceDemo {
	static Logger log=Logger.getLogger(ServiceDemo.class);
	public void test(int a){
		log.info(" ServiceDemo test "+a);
	}

}
